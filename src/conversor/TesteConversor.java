package conversor;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TesteConversor {
	
	private Conversão meuConversor;
	
    @Before
    public void setUp() throws Exception {
    meuConversor = new Conversão();
    }

	@Test
	public void testCelsiusParaFahrenheitComValorDouble() {
		assertEquals(104.0, meuConversor.celsiusParaFahrenheit(40.0), 1);
	}
	@Test
	public void testFahrenheitParaCelsiusComValorDouble(){
		assertEquals(-15.0, meuConversor.fahrenheitParaCelsius(5.0), 1);
	}
	@Test
	public void testKelvinParaCelsiusComValorDouble(){
		assertEquals(-223, meuConversor.kelvinParaCelsius(50), 1);
	}
	@Test
	public void testCelsiusParaKelvinComValorDouble(){
		assertEquals(15, meuConversor.celsiusParaKelvin(-258.15), 1);
	}
	@Test
	public void testKelvinParaFahrenheitComValorDouble(){
		assertEquals(15, meuConversor.kelvinParaFahrenheit(263.705556), 1);
	}
	@Test
	public void testFahrenheitParaKelvinComValorDouble(){
		assertEquals(263.705556, meuConversor.fahrenheitParaKelvin(15), 1);
	}
}
