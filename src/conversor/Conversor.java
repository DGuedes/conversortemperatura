package conversor;

import java.util.Scanner;  
import java.io.*;

public class Conversor {
	
	public static void main(String[] args) throws IOException{
		
		Scanner leitor = new Scanner(System.in);  
		int menuOpcao;
		double umValor;
		
		System.out.println("1. Celsius -> Fahrenheit");
		System.out.println("2. Fahrenheit -> Celsius");
		System.out.println("3. Kelvin -> Celsius");
		System.out.println("4. Celsius -> Kelvin");
		System.out.println("5. Kelvin -> Fahrenheit");
		System.out.println("6. Fahrenheit -> Kelvin");
		
		menuOpcao=leitor.nextInt();
		
		System.out.println("Debugando:");
		System.out.println("Valor:" + menuOpcao);		
		
		Conversão umaConversão = new Conversão();
		
		switch (menuOpcao){
		case 1:
			System.out.println("dentro do case 1!");
			System.out.println("Convertendo de Celsius para Fahrenheit.");
			System.out.println("Valor em Celsius:");
			umValor=leitor.nextDouble();
			System.out.println(umaConversão.celsiusParaFahrenheit(umValor));
			break;
		
		case 2:
			System.out.println("Convertendo de Fahrenheit para Celsius.");
			System.out.println("Valor em Fahrenheit:");
			umValor=leitor.nextDouble();
			System.out.println(umaConversão.fahrenheitParaCelsius(umValor));
			break;
			
		case 3:
			System.out.println("Convertendo de Kelvin para Celsius.");
			System.out.println("Valor em Kelvin:");
			umValor = leitor.nextDouble();
			System.out.println(umaConversão.kelvinParaCelsius(umValor));
			break;
		case 4:
			System.out.println("Convertendo de Celsius para Kelvin.");
			System.out.println("Valor em Celsius:");
			umValor = leitor.nextDouble();
			System.out.println(umaConversão.celsiusParaKelvin(umValor));
			break;
			
		case 5:
			System.out.println("Convertendo de Kelvin para Fahrenheit.");
			System.out.println("Valor em Kelvin:");
			umValor = leitor.nextDouble();
			System.out.println(umaConversão.kelvinParaFahrenheit(umValor));
			break;
		case 6:
			System.out.println("Convertendo de Fahrenheit para Kelvin.");
			System.out.println("Valor em Fahrenheit:");
			umValor = leitor.nextDouble();
			System.out.println(umaConversão.fahrenheitParaKelvin(umValor));
			break;		
			
		default:
			System.out.println("Erro! Opção invalida.");
			System.out.println("Nova opção:");
			System.out.println("1. Celsius -> Fahrenheit");
			System.out.println("2. Fahrenheit -> Celsius");
			System.out.println("3. Kelvin -> Celsius");
			System.out.println("4. Celsius -> Kelvin");
			System.out.println("5. Kelvin -> Fahrenheit");
			System.out.println("6. Fahrenheit -> Kelvin");
			menuOpcao=leitor.nextInt();		
		}

	}
}
